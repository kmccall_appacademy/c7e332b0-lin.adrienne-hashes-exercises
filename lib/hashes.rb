# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  hsh = {}
  str.split.each do |word|
    hsh[word] = word.length
  end
  hsh
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  largest_val = hash.values.max
  hash.key(largest_val)
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |key, val|
    older[key] = val
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  counter_hash = Hash.new(0)
  word.each_char do |ch|
    counter_hash[ch] += 1
  end
  counter_hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  uniq_hash = {}
  arr.each do |el|
    uniq_hash[el] = true
  end
  uniq_hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  counter_hash = Hash.new(0)
  numbers.each do |num|
    counter_hash[:even] += 1 if num.even?
    counter_hash[:odd] += 1 if num.odd?
  end
  counter_hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  counter_hash = Hash.new(0)

  string.each_char do |ch|
    counter_hash[ch] += 1 if "aeiou".include?(ch)
  end
  sorted = counter_hash.sort_by { |_key, val| val }
  sorted.last.first
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  second_half = students.select { |_name, bday| bday > 6 }

  names = second_half.keys
  res = []
  names.each.with_index do |_name, idx1|
    (idx1 + 1...names.length).each do |idx2|
      res << [names[idx1], names[idx2]]
    end
  end
  res
end


# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  counter_hash = Hash.new(0)

  specimens.each do |specimen|
    counter_hash[specimen] += 1
  end

  number_of_species = counter_hash.length
  smallest_population_size = counter_hash.values.min
  largest_population_size = counter_hash.values.max

  (number_of_species**2) * (smallest_population_size / largest_population_size)
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_count = character_count(normal_sign)
  vandal_count = character_count(vandalized_sign)

  normal_count.all? { |ch1, count| count >= vandal_count[ch1] }
end

def character_count(string)
  counter = Hash.new(0)
  string.delete('!?.,').downcase.chars { |ch| counter[ch] += 1 }
  counter
end
